// eslint-disable-next-line no-unused-vars
import { Chart as ChartJS } from "chart.js/auto"; // A ne pas commenter sinon les graphs ne fonctionne plu☼ 
import { Bar } from "react-chartjs-2";
import { addDays, format, parse } from 'date-fns';

function VoteBarChart(title, apiData, day) {
  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        text: title,
      },
    },
  };

  const parsedDay = parse(day, 'MM/dd/yy', new Date());

  const labels = Array.from({ length: 30 }, (_, index) => {
    const date = addDays(parsedDay, index); // Ajouter les jours pour obtenir les jours suivants
    return format(date, 'MM/dd/yy'); // Formatage de la date pour l'affichage
  });

  const data = labels.map(label => apiData[label] || 0);

    const datas = {
        labels,
        datasets: [
          {
            label: 'Nombre de propositions soumises au vote',
            data,
            borderColor: 'rgb(60, 255, 60)',
            backgroundColor: 'rgba(60, 255, 60, 0.5)',
          }
        ]
      };

    return ( 
        <div style={{ width: '50%', margin: 'auto' }}>
            <Bar datasetIdKey="Vote" options={options} data={datas} />
        </div>
    )
}

export default VoteBarChart