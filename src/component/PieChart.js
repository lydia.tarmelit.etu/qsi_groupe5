// eslint-disable-next-line no-unused-vars
import { Chart as ChartJS } from "chart.js/auto"; // A ne pas commenter sinon les graphs ne fonctionne plu☼ 
import { Pie } from "react-chartjs-2";

function PieChart(title, apiData) {

    const options = {
        responsive: true,
        plugins: {
          legend: {
            position: 'top',
          },
          title: {
            display: true,
            text: title,
          },
        },
    };

    let labels = Object.keys(apiData);
    let data = Object.values(apiData);

    const datas = {
        labels,
        datasets: [
          {
            label: ['Taux de yay','Taux de nay','Taux de pass'],
            data,
            borderColor: [
              'rgb(132, 255, 99)',
              'rgb(255, 99, 132)',
              'rgb(132, 99, 255)'
            ],
            backgroundColor: [
              'rgb(132, 255, 99, 0.5)',
              'rgb(255, 99, 132, 0.5)',
              'rgb(132, 99, 255, 0.5)'
            ],
          }
        ]
      };

    return ( 
        <div style={{ width: '30%', margin: 'auto' }}>
            <Pie datasetIdKey="Pie" options={options} data={datas} />
        </div>
    )
}

export default PieChart