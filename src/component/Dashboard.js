import { useEffect, useState } from "react"
import DAOLineChart from "./DAOLineChart"
import PieChart from "./PieChart"
import VoteBarChart from "./VoteBarGraph"
import { ButtonGroup, Dropdown, DropdownButton } from 'react-bootstrap';
import { generateData, generatePieData} from "./FalseDataGenerator";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const DashBoard = () => {
    const [line, setLine] = useState('')
    const [bar, setBar] = useState('')
    const [pie, setPie] = useState('')
    const [association, setAssociation] = useState([])
    const [currentAsso, setCurrentAsso] = useState('')
    const [currentPropo, setCurrentPropo] = useState('')
    const [startDate, setStartDate] = useState(new Date());

    const url = [ 'https://api.ghostnet.tzkt.io/v1/contracts/KT1QTwmF2eptKss2FWbT1rHP5wCERL16kihQ/storage']

    const getData = async () => {
        const data = await fetch( url[0], {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        });
        const response = await data.json()
        const objectsArray = Object.values(response);
        let listeAsso= []
        let day = startDate.getDate()
        if (day < 10) day = '0' + day
        let month = startDate.getMonth() + 1
        if (month < 10) month = '0' + month
        let year = startDate.getFullYear() - 2000
        const fullDate = month + '/' + day + '/' + year
        objectsArray.forEach(asso => {
            const assoData= {
                nom: asso.name,
                lineData: generateData(fullDate, 1000),
                barData: generateData(fullDate, 20),
                propositions: [ 
                    {nom: 'Proposition 1', pieData: generatePieData() },
                    {nom: 'Proposition 2', pieData: generatePieData() },
                    {nom: 'Proposition 3', pieData: generatePieData() }
                ]
            }
            listeAsso.push(assoData)
        });
        setAssociation(listeAsso)
    }

    const setDataAsso = (assoName) => {
        const asso = association.find(asso => asso.nom === assoName)
        let day = startDate.getDate()
        if (day < 10) day = '0' + day
        let month = startDate.getMonth() + 1
        if (month < 10) month = '0' + month
        let year = startDate.getFullYear() - 2000
        const fullDate = month + '/' + day + '/' + year
        setLine(DAOLineChart('Nombre de DAO token achetés sur un mois glissant', asso.lineData, fullDate))
        setBar(VoteBarChart('Nombre de propositions soumises au vote sur un mois glissant', asso.barData, fullDate))
        setPie('')
        setCurrentPropo('')
        setCurrentAsso(asso)
    }

    const setDataDate = (date) => {
        setStartDate(date)
        let day = date.getDate()
        if (day < 10) day = '0' + day
        let month = date.getMonth() + 1
        if (month < 10) month = '0' + month
        let year = date.getFullYear() - 2000
        const fullDate = month + '/' + day + '/' + year
        setLine(DAOLineChart('Nombre de DAO token achetés sur un mois glissant', generateData(fullDate, 1000), fullDate))
        setBar(VoteBarChart('Nombre de propositions soumises au vote sur un mois glissant', generateData(fullDate, 20), fullDate))
    }

    const setPieData = (propositions, propoName) => {
        const proposition = propositions.find(propo => propo.nom === propoName)
        setPie(PieChart('Taux de yay, nay ou pass pour la proposition', proposition.pieData))
        setCurrentPropo(proposition)
    }

    useEffect(() => {
        if (line === '' && bar === '' && pie === '') {
            getData()
        }
    }, [line, bar, pie])

    const showAssociation = () => {
        const result = []
        for (let asso in association){
            const eventKey = `${association[asso].nom}`
            const label = `${association[asso].nom}`
            result.push(
                <Dropdown.Item key={eventKey} eventKey={eventKey} onClick={() => setDataAsso(association[asso].nom)}>
                    {label}
                </Dropdown.Item>
            )
        }
        return result
    }

    const showProposition = (propositions) => {
        const result = []
        for (let index in propositions){
            const eventKey = `${propositions[index].nom}`
            const label = `${propositions[index].nom}`
            result.push(
                <Dropdown.Item key={eventKey} eventKey={eventKey} onClick={() => setPieData(propositions, propositions[index].nom)}>
                    {label}
                </Dropdown.Item>
            )
        }
        return result
    }

    return (<>
    <p>Liste des Associations</p>
    {currentAsso && <p>Association actuelle: {currentAsso.nom}</p>}
    <DropdownButton as={ButtonGroup} title="Pour l'association" id='association'>
        {showAssociation()}
    </DropdownButton>
    {currentAsso && <DatePicker selected={startDate} onChange={(date) => setDataDate(date)} />}
    <div className="d-flex justify-content-center align-items-center">
        {line}
        {bar}
    </div>
    {(line !== '' & bar !== '') &&
        <>
            <p>Liste des propositions</p>
            {currentPropo && <p>Proposition actuelle: {currentPropo.nom}</p>}
            <DropdownButton as={ButtonGroup} title="Pour la proposition" id='proposition'>
                {showProposition(currentAsso.propositions)}
            </DropdownButton>
        </>
    }
    {pie}
    </>)
}

export default DashBoard