import { format, addDays } from 'date-fns';

export function generateData(startDate, max) {
    let currentDate = startDate;
    const data = {};
    for (let i = 0; i < 30; i++) {
        const formattedDate = format(currentDate, 'MM/dd/yy');
        const randomValue = Math.floor(Math.random() * max); 
        data[formattedDate] = randomValue;
        currentDate = addDays(currentDate, 1);
    }
    return data;
}

export function generatePieData() {
    const testDataPie = {
        "Yay": Math.floor(Math.random() * 30),
        "Nay": Math.floor(Math.random() * 30),
        "Pass": Math.floor(Math.random() * 10)
    }
    return testDataPie
}

