const Graph = () => {
    const fakeData = [
        {
            type: "transaction", 
            id: 1, 
            level: 1, 
            timestamp: '',
            block: '',
            hash: '',
            counter: 1,
            initiator: {},
            sender: {},
            senerCodeHash: 2,
            nonce: 3,
            gasLimit: 200,
            gasUsed: 20,
            storageLimit: 10,
            storageUsed: 0,
            bakerFee: 4,
            storageFee: 4,
            allocationFee: 4,
            target: {},
            targerCodeHash: 4,
            amount: 4,
            parameter: {},
            storage: {},
            diffs: [{}],
            status: '',
            errors: [{}],
            hasInternals: false,
            tokenTransfersCount: 0,
            numActivations: 0
        }
    ]
}

export default Graph