// eslint-disable-next-line no-unused-vars
import { Chart as ChartJS } from "chart.js/auto"; // A ne pas commenter sinon les graphs ne fonctionne plu☼ 
import { Line } from "react-chartjs-2";
import { addDays, format, parse } from 'date-fns';

function DAOLineChart(title, apiData, day) {
    const options = {
        responsive: true,
        plugins: {
          legend: {
            position: 'top',
          },
          title: {
            display: true,
            text: title,
          },
        },
    };


    const parsedDay = parse(day, 'MM/dd/yy', new Date());

    const labels = Array.from({ length: 30 }, (_, index) => {
      const date = addDays(parsedDay, index); // Ajouter les jours pour obtenir les jours suivants
      return format(date, 'MM/dd/yy'); // Formatage de la date pour l'affichage
    });

    const data = labels.map(label => apiData[label] || 0);

    console.log(data, labels, apiData)

    const datas = {
        labels,
        datasets: [
          {
            label: 'Nombre de DAO token achetés',
            data,
            borderColor: 'rgb(60, 99, 255)',
            backgroundColor: 'rgba(60, 99, 255, 0.5)',
          }
        ]
      };

    return ( 
        <div style={{ width: '50%', margin: 'auto' }}>
            <Line datasetIdKey="DAO" options={options} data={datas} />
        </div>
    )
}

export default DAOLineChart